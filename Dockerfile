FROM node:9.9.0-alpine

MAINTAINER Vitaliy Kovalchuk <v.kovalchuk.vitaliy@gmail.com>

ENV NODE_ENV production
EXPOSE 8080
EXPOSE 5858

RUN mkdir -p /usr/src/app
RUN mkdir -p ~/.bcoin

WORKDIR /usr/src/app

COPY package.json /usr/src/app/

RUN cd /usr/src/app/
RUN yarn --pure-lockfile
RUN npm rebuild

COPY . /usr/src/app

CMD ["yarn", "start-debug"]
