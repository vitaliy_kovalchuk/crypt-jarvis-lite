import {Request} from "express";
import {ErrorCodes, HttpCodes, ValidationCodes} from "../../enum";
import {Result} from "express-validator/check/validation-result";
import {ResponseError} from "../../models";
import {validationResult} from "express-validator/check";
import {Observable} from "rxjs";
import {ValidationError} from "../../validator";

export abstract class AbstractController {

	protected validate(req: Request): Observable<any> {
		const result: Result<any> = validationResult(req);
		if (result.isEmpty()) {
			return Observable.of(AbstractController.getRequestData(req));
		} else {
			return Observable.throw(new ResponseError({
				code: ErrorCodes.VALIDATION_ERROR,
				httpCode: HttpCodes.BAD_REQUEST,
				message: "Data validation error",
				explanation: result.formatWith(this.errorFormatter()).array()
			}));
		}
	}

	protected static getRequestData(req: Request): any {
		return {...req.params, ...req.query, ...req.body};
	}

	private errorFormatter(): any {
		return ({location, msg, param, value, nestedErrors}) =>
			new ValidationError({
				param,
				error: msg,
				message: ValidationCodes.Utils.string(msg)
			});
	}
}
