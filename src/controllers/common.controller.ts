/**
 * @author Vitaliy Kovalchuk
 * Created on 01.03.2017.
 */
import {NextFunction, Request, Response} from "express";

import {ResponseError} from "../models";
import {ErrorCodes, HttpCodes} from "../enum";
import {AbstractController} from "./abstract";

const pkg = require("../../package.json");

class CommonController extends AbstractController {

	constructor() {
		super();
	}

	public getVersion(req: Request, res: Response, next: NextFunction): void {
		try {
			res.json({
				version: pkg.version
			});
		} catch (error) {
			next(new ResponseError({code: ErrorCodes.INTERNAL_ERROR, httpCode: HttpCodes.INTERNAL_ERROR}));
		}
	}

	public  ping(req: Request, res: Response, next: NextFunction): void {
		res.send("pong").end();
	}
}

export const commonController = new CommonController();
