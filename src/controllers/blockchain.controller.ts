import {Logger as log} from "../logger/index";
import * as bcoin from "bcoin";
import {Observable} from "rxjs";
import {config} from "../config";
import {NetworkInterface} from "../services/interface";
import {kafka, redis} from "../database";
import {Address, Block, Transaction} from "../models";

export class BlockchainController {

	private readonly networkService: NetworkInterface;
	private readonly redisKey: string;

	constructor(networkService: NetworkInterface) {
		this.networkService = networkService;
		this.redisKey = "height." + this.networkService.getCoinName();
		this.networkService
			.init()
			.subscribe(
				() => this.init(),
				(err) => log.error(`${this.getCoinName()}::Error while starting node service`, err)
			);
	}

	private init(): void {
		this.networkService.startSync();
		this.checkForGaps();

	}

	private checkForGaps(): void {
		log.info(`${this.getCoinName()}::Start checking for gaps`);
		Observable
			.fromPromise(redis.get(this.redisKey))
			.map((height: string) => +height)
			.subscribe((height: number) => {
					const tip = this.networkService.getTip();
					log.debug(`${this.getCoinName()}::Got last height from db: ${height}, form network:${JSON.stringify(tip)}`);
					if (height >= tip.height) {
						if (this.networkService.isSynced()) {
							log.info(`${this.getCoinName()}::Db are synchronized`);
							this.initEventsHandlers();
						} else {
							log.info(`${this.getCoinName()}::Mempool are not synced with network, wait for repeat 10s, current tip: ${this.networkService.getTip().height}`);
							setTimeout(() => this.checkForGaps(), 10000);
						}
					} else {
						log.info(`${this.getCoinName()}::Db are not synchronized`);
						this.fillGaps(height, tip.height);
					}
				}, error => {
					log.error(`${this.getCoinName()}::Error while checking for gaps, retry in 10s `, error);
					setTimeout(() => this.checkForGaps(), 10000);
				}
			);
	}

	private fillGaps(from: number, to: number): void {
		log.info(`${this.getCoinName()}::Start filling gaps, from: ${from}, to: ${to}`);
		Observable
			.range(+from, +to - +from)
			.concatMap((height: number) => this.networkService.getBlock(height).switchMap(block => this.blockHandler(block)))
			.subscribe(
				(data: { hash: string, height: number }) => log.info(`${this.getCoinName()}::Saved new block with \
				\ height: ${data.height}, hash: ${data.hash}`),
				error => log.error(`${this.getCoinName()}::Error while filling gaps,`, error) || console.error(error),
				() => this.checkForGaps()
			);
	}

	private initEventsHandlers(): void {
		this.networkService.on(NetworkEvents.BLOCK, this.onBlockEvent);
		this.networkService.on(NetworkEvents.TRANSACTION, this.onTransactionHandler);
	}

	private blockHandler(data: bcoin.block): Observable<{ hash: string, height: number }> {
		const block: Block = Block.parseBcoinBlock(data);
		const txs: Transaction[] = data.txs.map(t => Transaction.bcoinTxToTransactionModel(t, block));
		const addresses: Address[] = Address.parseTransactionAddresses(txs);

		return Observable
			.create((observer) => {
				kafka
					.producer
					.send([{
						topic: `addresses.${this.getCoinName()}`,
						messages: addresses.map(a => a.toJSON())
					}], (err, data) => {
						if (err) {
							observer.error(err);
						} else {
							observer.next(data);
						}
					});
			})
			.map(() => ({hash: block.hash, height: block.height}))
			.do(data => redis.set(this.redisKey, data.height));
	}

	private transactionHandler(tx: bcoin.tx): Observable<{ hash: string }> {
		const addresses: Address[] = Address.parseTransactionAddresses([tx]);
		return Observable
			.create((observer) => {
				kafka
					.producer
					.send([{
						topic: `addresses.${this.getCoinName()}`,
						messages: addresses.map(a => a.toJSON())
					}], (err, data) => {
						if (err) {
							observer.error(err);
						} else {
							observer.next(data);
						}
					});
			})
			.map(() => ({hash: tx.hash}));
	}

	private onBlockEvent(data: bcoin.block): void {
		this.blockHandler(data)
			.subscribe(
				(data: { hash: string, height: number }) => log.info(`${this.getCoinName()}::Saved new block with \
				\ height: ${data.height}, hash: ${data.hash}`),
				error => {
					log.error(`${this.getCoinName()}::Error while filling gaps,`, error);
					this.networkService.stopSync();
				}
			);
	}

	private onTransactionHandler(data: bcoin.block): void {
		this.transactionHandler(data)
			.subscribe(
				(data: { hash: string }) => log.info(`${this.getCoinName()}::Saved new transaction with  hash: ${data.hash}`),
				error => {
					log.error(`${this.getCoinName()}::Error while handling new transaction,`, error);
					this.networkService.stopSync();
				}
			);
	}

	private getCoinName(): string {
		return this.networkService.getCoinName();
	}
}

enum NetworkEvents {
	BLOCK = "block",
	TRANSACTION = "tx"
}
