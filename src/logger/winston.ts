/**
 * @author Vitaliy Kovalchuk
 * Created on 25.01.2017.
 */
import {LoggerInstance, Logger as Winston, transports} from "winston";
import {config} from "../config";
import * as fs from "fs";

class LoggerClass {

	public logger: LoggerInstance;

	constructor() {
		this.init();
	}

	private init(): void {
		const dir = "logs";
		if (!fs.existsSync(dir)) {
			fs.mkdirSync(dir);
		}
		const logLevel = config.get("logger").level;
		this.logger = new Winston({
			level: logLevel,
			transports: [
				new (transports.File)({name: "combined", filename: "logs/combined.log"}),
				new (transports.File)({name: "error", filename: "logs/error.log", level: "error"}),
				new (transports.Console)({
					name: "console",
					handleExceptions: true,
					humanReadableUnhandledException: true,
					colorize: true,
					timestamp: true,
					level: logLevel
				})
			]
		});
	}
}

export const IS_DEBUG: boolean = config.get("logger").level.toLowerCase() === "debug";
export const Logger = new LoggerClass().logger;
