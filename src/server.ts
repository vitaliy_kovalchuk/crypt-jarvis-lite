/**
 * @author Vitaliy Kovalchuk, create on 21.02.17.
 * @email v.kovalchuk.vitaliy@gmail.com
 */

import {Application} from "express";
import * as http from "http";

import {config} from "./config";
import {Logger as log} from "./logger";
import {app} from "./app";

export class Server {

	constructor(private port: number = config.get("port"), private application: Application = app) {
		this.init();
	}

	private init() {
		log.info("Server init");
		const server: http.Server = http.createServer(this.application);
		server.listen(this.port);
		server.on("error", this.onError());
		server.on("listening", this.onListening(server));
	}

	private onError() {
		return (error: NodeJS.ErrnoException) => {
			if (error.syscall !== "listen") {
				throw error;
			}
			const bind = "Port " + this.port;
			switch (error.code) {
				case "EACCES":
					log.error(bind + " requires elevated privileges");
					process.exit(1);
					break;
				case "EADDRINUSE":
					log.error(bind + " hasRole already in use");
					process.exit(1);
					break;
				default:
					throw error;
			}
		};
	}

	private onListening(server: http.Server) {
		return () => {
			const addr = server.address();
			const bind = "port " + addr.port;
			log.info("Application start up on " + bind);
			log.debug(`\n\nApplication version: ${require("../package.json").version} start up with config \n${config}`);
		};
	}
}

export const server = new Server();
