/**
 * @author Vitaliy Kovalchuk, create on 23.02.17.
 * @email v.kovalchuk.vitaliy@gmail.com
 */

import {config} from "../config";
import {Client, HighLevelProducer} from "kafka-node";
import {Logger as log} from "../logger";

class Kafka {

	private readonly client: Client;
	private readonly highLevelProducer: HighLevelProducer;

	constructor() {
		this.client = new Client(config.get("zookeeper").url);
		this.highLevelProducer = new HighLevelProducer(this.client);

		this.highLevelProducer.on("ready", () => {
			log.info(`Producer is ready`);
		});

		this.highLevelProducer.on("error", (err) => {
			log.info(`Error while starting producer`, err);
		});
	}

	public get producer(): HighLevelProducer {
		return this.highLevelProducer;
	}

}

export const kafka = new Kafka();
