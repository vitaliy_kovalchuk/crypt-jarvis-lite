import * as IORedis from "ioredis";
import {Redis} from "ioredis";

import {config} from "../config";
import {Logger as log} from "../logger";

export class RedisDB {

	private readonly _redis: Redis;

	constructor() {
		const url = config.get("redis").url;
		this._redis = new IORedis(url);
		log.debug(`Connect to redis via url: ${url}`);
	}

	get redis() {
		return this._redis;
	}

}

export const redis = new RedisDB().redis;
