/**
 * @author Vitaliy Kovalchuk
 * Created on 23.01.2017.
 */

import * as convict from "convict";

const defaultConfig = {
	port: 8080,
	logger: {
		level: "info"
	},
	node: {
		baseFolder: "./.blockchain",
	},
	redis: {
		url: "redis://127.0.0.1:6379"
	},
	zookeeper: {
		url: "localhost:9092"
	},
	swagger: {
		basePath: "/"
	}
};

const schema: convict.Schema = {
	env: {
		doc: "Specifies application environment",
		format: ["development", "deployment", "production"],
		default: "development",
		env: "NODE_ENV"
	},
	port: {
		doc: "Specifies port for application start up",
		format: "port",
		default: defaultConfig.port,
		env: "PORT"
	},
	redis: {
		url: {
			doc: "Specifies url for redis connection",
			format: "url",
			default: defaultConfig.redis.url,
			env: "REDIS_URL",
		}
	},
	node: {
		dataFolder: {
			doc: "Specifies base folder for databases storage",
			default: defaultConfig.node.baseFolder,
			env: "BASE_FOLDER"
		},
	},
	swagger: {
		basePath: {
			doc: "The base path on which the API is served, which is relative to the host. If it is not included, the API is served directly under the host.",
			format: String,
			default: defaultConfig.swagger.basePath,
			env: "SWAGGER_PATH"
		}
	},
	zookeeper: {
		url: {
			doc: "Specifies url for zookeeper instance",
			format: "url",
			default: defaultConfig.zookeeper.url,
			env: "ZOOKEEPER_URL"
		}
	},
	logger: {
		level: {
			doc: "Specifies log level for application logs",
			format: ["error", "warn", "info", "verbose", "debug", "silly"],
			default: defaultConfig.logger.level,
			env: "LOGGER_LEVEL"
		}
	}
};

const Config: convict.Config = convict(schema);
export const config = Config;
