/**
 * @author Vitaliy Kovalchuk
 * Created on 18.02.2017.
 */

export * from "./swagger.router";
export * from "./common.router";
