"use strict";
import {NextFunction, Request, Response} from "express";
import * as swagger from "swagger-jsdoc";

import {AbstractRouter} from "./abstract";
import {config} from "../config";
import * as swaggerUi from "swagger-ui-express";

const pkg = require("../../package.json");

class SwaggerRouter extends AbstractRouter {

	constructor() {
		super();
		this.init();
	}

	public init(): void {
		const swaggerJson: any = swagger(SwaggerRouter.buildSwaggerOptions());
		this.router.get("/api.json", this.swaggerJsonFormatter(swaggerJson));
		this.router.use(swaggerUi.serve, swaggerUi.setup(swaggerJson));
	}

	private swaggerJsonFormatter(swaggerJson: any) {
		return (req: Request, res: Response, next: NextFunction) => res.json(swaggerJson);
	}

	private static buildSwaggerOptions(): object {
		return {
			swaggerDefinition: {
				info: {
					title: "Crypto-vision",
					version: pkg.version
				},
				basePath: config.get("swagger").basePath,
				consumes: [
					"application/json"
				],
				produces: [
					"application/json"
				]
			},
			apis: [
				"./src/**/**.ts",
				"./dist/**/**.js"
			]
		};
	}
}

export const swaggerRouter = new SwaggerRouter();
