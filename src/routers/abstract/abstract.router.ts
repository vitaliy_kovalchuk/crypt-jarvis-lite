/**
 * @author Vitaliy Kovalchuk, create on 02.03.17.
 * @email v.kovalchuk.vitaliy@gmail.com
 */

import {Router} from "express";

export abstract class AbstractRouter {

	protected _router: Router;

	protected constructor() {
		this._router = Router();
	}

	get router(): Router {
		return this._router;
	}
}
