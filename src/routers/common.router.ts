/**
 * @author Vitaliy Kovalchuk
 * Created on 18.02.2017.
 */
"use strict";
import {AbstractRouter} from "./abstract";
import {commonController} from "../controllers";

/**
 * @swagger
 * tags:
 *   name: Common
 *   description: Base information about project
 * definitions:
 *   Version:
 *     type: object
 *     required:
 *       - version
 *     properties:
 *       version:
 *         type: string
 */

class CommonRouter extends AbstractRouter {

	constructor() {
		super();
		this.init();
	}

	protected init(): void {
		/**
		 * @swagger
		 * /ping:
		 *   get:
		 *     description: Returns string "pong
		 *     tags:
		 *       - Common
		 *     produces:
		 *       - plain/text
		 *     responses:
		 *       200:
		 *         description: Ok
		 */
		this.router.get("/ping", commonController.ping);
		/**
		 * @swagger
		 * /version:
		 *   get:
		 *     description: Returns project version
		 *     tags:
		 *       - Common
		 *     produces:
		 *       - application/json
		 *     responses:
		 *       200:
		 *         description: Project version
		 *         schema:
		 *           type: object
		 *           $ref: '#/definitions/Version'
		 */
		this.router.get("/version", commonController.getVersion);
	}
}

export const commonRouter = new CommonRouter();
