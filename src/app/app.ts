/**
 * @author Vitaliy Kovalchuk, create on 21.02.17.
 * @email v.kovalchuk.vitaliy@gmail.com
 */

import * as bodyParser from "body-parser";
import * as compression from "compression";
import * as cookieParser from "cookie-parser";
import * as express from "express";
import {NextFunction, Request, Response} from "express";
import * as partials from "express-partials";
import * as helmet from "helmet";
import * as morgan from "morgan";
import {internalError, ResponseError} from "../models";
import {ErrorCodes, HttpCodes} from "../enum";
import {Logger as log} from "../logger";
import {commonRouter, swaggerRouter} from "../routers";
import {BlockchainController} from "../controllers";
import {TestnetService} from "../services";

export class App {

	public express: express.Application;

	constructor() {
		this.express = express();
		this.middleware();
		this.routers();

		this.initBlockchainControllers();
	}

	private middleware(): void {
		this.express.use(helmet());
		this.express.use(morgan("combined"));
		this.express.use(partials());
		this.express.use(cookieParser());
		this.express.use(bodyParser.json());
		this.express.use(bodyParser.urlencoded({extended: false}));
		this.express.use(compression());
	}

	private routers(): void {
		this.express.use("/", commonRouter.router);
		this.express.use("/swagger", swaggerRouter.router);
		this.express.use((req: Request, res: Response) => {
			const error = new ResponseError({code: ErrorCodes.NOT_FOUND, httpCode: HttpCodes.NOT_FOUND});
			res
				.status(error.httpCode)
				.send(error.toJSON())
				.end();
		});
		this.express.use((error: any, req: Request, res: Response, next: NextFunction) => {
			if (error instanceof ResponseError) {
				res
					.status(error.httpCode)
					.send(error.toJSON())
					.end();
			} else {
				log.error("Unknown error", JSON.stringify(error));
				res
					.status(internalError.httpCode)
					.send(internalError.toJSON())
					.end();
			}
		});
	}

	private initBlockchainControllers() {
		const testnetController: BlockchainController = new BlockchainController(new TestnetService());
	}
}

export const app = new App().express;
