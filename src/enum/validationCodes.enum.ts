/**
 * @author Vitaliy Kovalchuk
 * Created on 21.02.2017.
 */
import {EnumUtils} from "./enum.utils";

/**
 * @swagger
 * definitions:
 *   ValidationCodes:
 *     type: integer
 *     format: int32
 *     enum:
 *       - 1010
 *       - 1020
 *       - 1030
 *       - 1040
 *       - 1050
 *       - 1060
 *   ValidationMessages:
 *     type: string
 *     enum:
 *       - REQUIRED
 *       - WRONG_TYPE
 *       - WRONG_FORMAT
 *       - MIN_VALUE
 *       - MAX_VALUE
 *       - OUT_OF_RANGE
 * */
export enum ValidationCodes {
	REQUIRED = 1010,
	WRONG_TYPE = 1020,
	WRONG_FORMAT = 1030,
	MIN_VALUE = 1040,
	MAX_VALUE = 1050,
	OUT_OF_RANGE = 1060
}

export namespace ValidationCodes {
	export const Utils = new EnumUtils(ValidationCodes);
}
