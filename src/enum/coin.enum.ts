import {EnumUtils} from "./enum.utils";

/**
 * @swagger
 * definitions:
 *   Coin:
 *     type: string
 *     enum:
 *       - btc
 *       - testnet
 *       - ltc
 *       - doge
 *       - dsh
 *       - btg
 * */
export enum Coin {
	BTC = 0,
	TESTNET = 1,
	LTC = 2,
	DOGE = 3,
	DSH = 5,
	BTG = 156
}

export namespace Coin {
	export const Utils = new EnumUtils(Coin);
}
