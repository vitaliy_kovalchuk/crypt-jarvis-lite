/**
 * @author Vitaliy Kovalchuk
 * Created on 21.02.2017.
 */
import {EnumUtils} from "./enum.utils";

/**
 * @swagger
 * responses:
 *   204:
 *     description: No content
 *   400:
 *     description: Bad request
 *     schema:
 *       $ref: '#/definitions/ValidationResponseError'
 *   401:
 *     description: Access denied
 *     schema:
 *       $ref: '#/definitions/ResponseError'
 *   404:
 *     description: Entity not found
 *     schema:
 *       $ref: '#/definitions/ResponseError'
 *   406:
 *     description: Not acceptable
 *     schema:
 *       $ref: '#/definitions/ResponseError'
 *   409:
 *     description: Conflict occurred while processing data
 *     schema:
 *       $ref: '#/definitions/ResponseError'
 *   500:
 *     description: Internal error occurred while processing data
 *     schema:
 *       $ref: '#/definitions/ResponseError'
 * definitions:
 *   HttpCodes:
 *     type: integer
 *     format: int32
 *     enum:
 *       - 200
 *       - 204
 *       - 400
 *       - 401
 *       - 403
 *       - 404
 *       - 406
 *       - 409
 *       - 500
 *   HttpMessages:
 *     type: string
 *     enum:
 *       - SUCCESS
 *       - NO_CONTENT
 *       - BAD_REQUEST
 *       - ACCESS_DENIED
 *       - FORBIDDEN
 *       - NOT_FOUND
 *       - NOT_ACCEPTABLE
 *       - CONFLICT
 *       - INTERNAL_ERROR
 * */
export enum HttpCodes {
	SUCCESS = 200,
	NO_CONTENT = 204,
	BAD_REQUEST = 400,
	ACCESS_DENIED = 401,
	FORBIDDEN = 403,
	NOT_FOUND = 404,
	NOT_ACCEPTABLE = 406,
	CONFLICT = 409,
	INTERNAL_ERROR = 500
}

export namespace HttpCodes {
	export const Utils = new EnumUtils(HttpCodes);
}
