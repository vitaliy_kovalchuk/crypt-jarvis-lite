/**
 * @author Vitaliy Kovalchuk
 * Created on 16.04.2017.
 */

export class EnumUtils {

	private _names: string[] = Object
		.keys(this.type)
		.filter(key => !EnumUtils.isIndex(key));

	private _indices: number[] = Object
		.keys(this.type)
		.filter(key => EnumUtils.isIndex(key))
		.map(index => Number(index));

	constructor(private type: any) {
		// Empty
	}

	public names(): string[] {
		return this._names;
	}

	public indices(): number[] {
		return this._indices;
	}

	public number(instance: any): number {
		instance = typeof instance === "string" ? instance.toUpperCase().trim() : undefined;
		return this.type[instance];
	}

	public string(instance: any): string {
		instance = typeof instance === "number" ? instance : undefined;
		return this.type[instance];
	}

	private static isIndex(key): boolean {
		const n = ~~Number(key);
		return String(n) === key && n >= 0;
	}
}
