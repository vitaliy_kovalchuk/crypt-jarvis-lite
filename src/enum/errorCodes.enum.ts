/**
 * @author Vitaliy Kovalchuk
 * Created on 21.02.2017.
 */
import {EnumUtils} from "./enum.utils";

/**
 * @swagger
 * definitions:
 *   ErrorCodes:
 *     type: integer
 *     format: int32
 *     enum:
 *       - 200
 *       - 204
 *       - 400
 *       - 401
 *       - 403
 *       - 404
 *       - 406
 *       - 409
 *       - 500
 *       - 1000
 *   ErrorMessages:
 *     type: string
 *     enum:
 *       - SUCCESS
 *       - NO_CONTENT
 *       - BAD_REQUEST
 *       - ACCESS_DENIED
 *       - FORBIDDEN
 *       - NOT_FOUND
 *       - NOT_ACCEPTABLE
 *       - CONFLICT
 *       - INTERNAL_ERROR
 * */
export enum ErrorCodes {
	SUCCESS = 200,
	NO_CONTENT = 204,
	BAD_REQUEST = 400,
	ACCESS_DENIED = 401,
	FORBIDDEN = 403,
	NOT_FOUND = 404,
	NOT_ACCEPTABLE = 406,
	CONFLICT = 409,
	INTERNAL_ERROR = 500,
	VALIDATION_ERROR = 1000
}

export namespace ErrorCodes {
	export const Utils = new EnumUtils(ErrorCodes);
}
