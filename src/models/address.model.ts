import {Model} from "./model.abstract";
import {isNullOrUndefined} from "util";
import {Transaction, TransactionInput, TransactionOutput} from "./index";

export class Address implements Model {

	public blockHash: string;
	public blockHeight: number;
	public transactionHash: string;
	public transactionIndex: number;
	public address: string;
	public amount: number;

	constructor(data: {
		blockHash?: string
		blockHeight?: number;
		transactionHash: string;
		transactionIndex: number;
		address: string;
		amount: number;
	}) {
		for (const prop in data) {
			this[prop] = data[prop];
		}
	}

	public static parseTransactionAddresses(transactions: Transaction[]): Address[] {
		const addresses: Address[] = [];
		transactions.map((tx: Transaction) => {
			addresses.push(...tx.inputs.map((input: TransactionInput) => {
				if (isNullOrUndefined(input.address)) {
					return;
				} else {
					return new Address({
						blockHash: tx.blockHash,
						blockHeight: tx.blockHeight,
						transactionHash: tx.hash,
						transactionIndex: tx.blockIndex,
						address: input.address,
						amount: -input.value
					});
				}
			}));
			addresses.push(...tx.outputs.map((output: TransactionOutput) => {
				if (isNullOrUndefined(output.address)) {
					return;
				} else {
					return new Address({
						blockHash: tx.blockHash,
						blockHeight: tx.blockHeight,
						transactionHash: tx.hash,
						transactionIndex: tx.blockIndex,
						address: output.address,
						amount: output.value
					});
				}
			}));
		});
		return addresses.filter((address) => !isNullOrUndefined(address));
	}

	public values(): object {
		return {
			blockHash: this.blockHash,
			blockHeight: this.blockHeight,
			transactionHash: this.transactionHash,
			transactionIndex: this.transactionIndex,
			address: this.address,
			amount: this.amount
		};
	}

	public toJSON(stringify: boolean = false): object | string {
		const res: any = this.values();
		return stringify ? JSON.stringify(res) : res;
	}
}
