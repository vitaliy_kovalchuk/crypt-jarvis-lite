import {Model} from "./model.abstract";
import * as bcoin from "bcoin";

export class TransactionOutput implements Model {

	public index: number;
	public value: number;
	public script: string;
	public address: string;
	public scriptType: number;

	constructor(data: {
		index: number
		value: number
		script: string
		address: string,
		scriptType: number
	}) {
		if (typeof data === "string") {
			data = JSON.parse(data);
		}
		for (const prop in data) {
			this[prop] = data[prop];
		}
	}

	public static parseBcoinOutput(data: any, index: number): TransactionOutput {
		const bcoinOutput: bcoin.output = bcoin.output.fromJSON(data);
		const jsonData: any = bcoinOutput.toJSON();
		return new TransactionOutput({
			index,
			value: jsonData.value,
			script: jsonData.script,
			address: jsonData.address,
			scriptType: bcoinOutput.script.getType()
		});
	}

	public values(): object {
		return {
			index: this.index,
			value: this.value,
			script: this.script,
			address: this.address,
			scriptType: this.scriptType
		};
	}

	public toJSON(stringify: boolean = false): object | string {
		const res: object = this.values();
		return stringify ? JSON.stringify(res) : res;
	}
}
