export abstract class Model {

	public abstract values(): object;

	public abstract toJSON(): object | string;
}
