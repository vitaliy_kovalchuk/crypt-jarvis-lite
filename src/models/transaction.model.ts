import * as moment from "moment";
import {Moment} from "moment";
import {TransactionInput} from "./transactionInput.model";
import {TransactionOutput} from "./transactionOutput.model";
import {Logger as log} from "../logger";
import {Model} from "./model.abstract";
import {isNullOrUndefined} from "util";
import {Block} from "./index";

export class Transaction implements Model {

	public blockHeight: number;
	public blockHash: string;
	public blockIndex: number;
	public hash: string;
	public size: number;
	public time: Moment;
	public version: number;
	public lockTime: number;
	public doubleSpend: boolean;
	public inputsCount: number;
	public outputsCount: number;
	public inputs: TransactionInput[];
	public outputs: TransactionOutput[];
	public rbf: boolean;

	constructor(data: {
		blockHeight?: number,
		blockHash?: string,
		blockIndex?: number,
		hash: string,
		size: number,
		time: Moment,
		version: number,
		lockTime: number,
		doubleSpend: boolean,
		doubleSpendOf?: string,
		inputsCount: number,
		outputsCount: number,
		inputs: TransactionInput[],
		outputs: TransactionOutput[],
		rbf?: boolean
	}) {
		for (const prop in data) {
			this[prop] = data[prop];
		}
		if (!moment.isMoment(this.time)) {
			if (moment(this.time).isValid()) {
				this.time = moment(this.time);
			} else {
				log.error("Can not init Transaction, because of invalid time format", this.time);
			}
		}
		this.inputs = this.inputs.map((input: TransactionInput) => input instanceof TransactionInput ?
			input :
			new TransactionInput(input));
		this.outputs = this.outputs.map((output: TransactionOutput) => output instanceof TransactionOutput ?
			output :
			new TransactionOutput(output));
	}

	public static bcoinTxToTransactionModel(tx: any, block: Block): TransactionModel {
		const json: string = typeof tx === "string" ? tx : JSON.stringify(tx);
		return {
			blockHeight: block.height,
			blockHash: block.hash,
			blockIndex: tx.index,
			hash: tx.hash,
			json,
			time: tx.time ? moment.unix(tx.time) : isNullOrUndefined(block) ? undefined : block.time,
		};
	}

	public values(): object {
		return {
			blockHeight: this.blockHeight || undefined,
			blockHash: this.blockHash || undefined,
			blockIndex: this.blockIndex || undefined,
			hash: this.hash,
			size: this.size,
			time: this.time.toISOString(),
			version: this.version,
			lockTime: this.lockTime,
			doubleSpend: this.doubleSpend,
			inputsCount: this.inputsCount,
			outputsCount: this.outputsCount,
			inputs: this.inputs.map(input => input.values()),
			outputs: this.outputs.map(output => output.values()),
			rbf: this.rbf || undefined,
		};
	}

	public toJSON(stringify: boolean = false): object | string {
		const res: object = this.values();
		return stringify ? JSON.stringify(res) : res;
	}
}

export interface TransactionModel {
	blockHeight: number;
	blockHash: string;
	blockIndex: number;
	hash: string;
	json: string;
	time: Moment;
}
