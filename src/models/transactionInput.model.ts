import {Model} from "./model.abstract";
import * as bcoin from "bcoin";

export class TransactionInput implements Model {

	public sequence: number;
	public witness: number;
	public index: number;
	public previousHash: string;
	public outputIndex: number;
	public address: string;
	public value: number;
	public script: string;
	public previousScript: string;
	public scriptType: number;

	constructor(data: {
		index: number
		previousHash: string
		outputIndex: number
		address: string
		value: number
		script: string,
		previousScript: string,
		scriptType: number
		sequence: number,
		witness: string
	}) {
		if (typeof data === "string") {
			data = JSON.parse(data);
		}
		for (const prop in data) {
			this[prop] = data[prop];
		}
	}

	public static parserBcoinInput(data: any, index: number): TransactionInput {
		const bcoinInput: bcoin.input = bcoin.input.fromJSON(data);
		const jsonData = bcoinInput.toJSON();
		return new TransactionInput({
			index,
			previousHash: jsonData.prevout.hash,
			outputIndex: jsonData.prevout.index,
			address: data.coin ? data.coin.address : jsonData.address,
			value: data.coin ? data.coin.value : undefined,
			script: jsonData.script,
			previousScript: data.coin ? data.coin.script : undefined,
			scriptType: bcoinInput.script.getType(),
			sequence: data.sequence,
			witness: data.witness
		});
	}

	public values(): object {
		return {
			index: this.index,
			previousHash: this.previousHash,
			outputIndex: this.outputIndex,
			address: this.address,
			value: this.value,
			script: this.script,
			previousScript: this.previousScript,
			scriptType: this.scriptType,
			sequence: this.sequence,
			witness: this.witness
		};
	}

	public toJSON(stringify: boolean = false): object | string {
		const res: object = this.values();
		return stringify ? JSON.stringify(res) : res;
	}
}
