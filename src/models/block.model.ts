import * as moment from "moment";
import {Logger as log} from "../logger";
import {Model} from "./model.abstract";
import * as bcoin from "bcoin";

export class Block implements Model {

	public hash: string;
	public height: number;
	public total: number;
	public fees: number;
	public size: number;
	public version: number;
	public time: moment.Moment;
	public bits: number;
	public nounce: number;
	public transactionNumber: number;
	public previousBlock: number;
	public merkleRoot: string;
	public transactions: string[];

	constructor(data: {
		hash: string
		height: number
		total: number
		fees: number
		size: number
		version: number
		time: moment.Moment
		bits: number
		nounce: number
		transactionNumber: number
		previousBlock: number
		merkleRoot: string
		transactions: string[]
	}) {
		for (const prop in data) {
			this[prop] = data[prop];
		}
		if (!moment.isMoment(this.time)) {
			if (moment(this.time).isValid()) {
				this.time = moment(this.time);
			} else {
				log.error("Can not init Block, because of invalid time format", this.time);
			}
		}
	}

	public static parseBcoinBlock(data: any): Block {
		const bcoinBlock: bcoin.block = bcoin.block.fromJSON(data);
		return new Block({
				hash: data.hash,
				height: data.height,
				total: this.calculateBlockTotal(data.txs),
				fees: this.calculateBlockFee(data.txs),
				size: bcoinBlock.getSize(),
				version: data.version,
				time: moment.unix(data.time),
				bits: data.bits,
				nounce: data.nonce,
				transactionNumber: data.txs ? data.txs.length : 0,
				transactions: data.txs ? data.txs.map(tx => tx.hash) : [],
				previousBlock: data.prevBlock,
				merkleRoot: data.merkleRoot
			}
		);
	}

	private static calculateBlockTotal(transactions: any): number {
		return transactions.map(tx => bcoin.tx.fromJSON(tx).getOutputValue()).reduce((acc, x) => acc + x);
	}

	private static calculateBlockFee(transactions: any): number {
		return transactions.map(tx => +tx.fee).reduce((acc, x) => acc + x);
	}

	public values(): object {
		return {
			hash: this.hash,
			height: this.height,
			total: this.total,
			fees: this.fees,
			size: this.size,
			version: this.version,
			time: this.time.toISOString(),
			bits: this.bits,
			nounce: this.nounce,
			transactionNumber: this.transactionNumber,
			previousBlock: this.previousBlock,
			merkleRoot: this.merkleRoot,
			transactions: this.transactions
		};
	}

	public toJSON(stringify: boolean = false): object | string {
		const res: object = this.values();
		return stringify ? JSON.stringify(res) : res;
	}

	private static blocksMapper(row: any, index: number, data: any[]): any {
		return this.blockMapper(data[index]);
	}

	private static blockMapper(data: any): Block {
		return new Block({
			hash: data.hash,
			height: +data.height,
			total: +data.total,
			fees: +data.fees,
			size: +data.size,
			version: +data.version,
			time: moment(data.time),
			bits: +data.bits,
			nounce: +data.nounce,
			transactionNumber: +data.transaction_number,
			previousBlock: data.previous_block,
			merkleRoot: data.merkle_root,
			transactions: data.transactions
		});
	}
}
