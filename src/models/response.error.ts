/**
 * @author Vitaliy Kovalchuk
 * Created on 21.02.2017.
 */
import * as moment from "moment";
import {ErrorCodes, HttpCodes} from "../enum";
import {ValidationError} from "../validator";

export class ResponseError {

    public code: ErrorCodes;
    public httpCode: HttpCodes;
    public error: string;
    public message: string;
    public timestamp: number;
    public explanation: ValidationError[];

    constructor(error: Error) {
        this.code = error.code;
        this.httpCode = error.httpCode;
        this.error = ErrorCodes.Utils.string(this.code) || HttpCodes.Utils.string(this.code);
        this.message = error.message;
        this.timestamp = moment().valueOf();
        this.explanation = error.explanation;
    }

    public toJSON(): object {
        const res: Response = {
            code: this.code,
            error: this.error,
            timestamp: this.timestamp
        };
        if (this.message) {
            res.message = this.message;
        }
        if (Array.isArray(this.explanation)) {
            res.explanation = this.buildExplanationObject();
        }
        return res;
    }

    private buildExplanationObject(): ValidationError[] {
        return this.explanation.map((exp: ValidationError): ValidationError => {
            return {
                param: exp.param,
                error: exp.error,
                message: exp.message
            } as any;
        });
    }
}

export const internalError = new ResponseError({
    httpCode: HttpCodes.INTERNAL_ERROR,
    code: ErrorCodes.INTERNAL_ERROR,
    message: "Internal server error"
});

interface Response {
    code: number;
    error: string;
    timestamp: number;
    message?: string;
    explanation?: ValidationError[];
}

interface Error {
    code: ErrorCodes;
    httpCode: HttpCodes;
    message?: string;
    explanation?: ValidationError[];
}
