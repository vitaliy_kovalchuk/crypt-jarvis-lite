/**
 * @author Vitaliy Kovalchuk
 * Created on 11.03.2017.
 */
import {ValidationCodes} from "../enum";
/**
 * @swagger
 * definitions:
 *   ValidationError:
 *     required:
 *       - param
 *       - error
 *       - message
 *     type: object
 *     properties:
 *       param:
 *         type: string
 *       error:
 *         $ref: "#/definitions/ValidationCodes"
 *       message:
*         $ref: "#/definitions/ValidationMessages"
* */

export class ValidationError {

	public param: string;
	public error: ValidationCodes;
	public message: string;
	public args?: any;

	constructor(data: { param: string, error: ValidationCodes, message?: string, args?: any }) {
		this.param = data.param;
		this.error = data.error;
		this.message = data.message;
		this.args = data.args;
	}

	public toJson(): object {
		const res: any = {
			code: this.param,
			error: this.error
		};
		if (this.message) {
			res.message = this.message;
		}
		if (this.args) {
			res.args = this.args;
		}
		return res;
	}

	public tostring(): object {
		return this.toJson();
	}
}
