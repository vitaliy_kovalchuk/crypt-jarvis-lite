import {fullnode as Node, util} from "bcoin";
import {Logger as log} from "../logger";
import {config} from "../config";
import {Observable} from "rxjs";
import {NetworkInterface} from "./interface";
import * as bcoin from "bcoin";
import {Coin} from "../enum";
import * as fs from "fs";

export class TestnetService implements NetworkInterface {

	private node: Node;

	private coin: Coin = Coin.TESTNET;
	private coinName: string = Coin.Utils.string(this.coin);

	constructor() {
		// Empty
	}

	public init(): Observable<Node> {
		this.node = new Node(this.getConfig());
		this.node.on("error", log.error);
		return Observable.fromPromise(this.startNode());
	}

	private async startNode(): Promise<Node> {
		await this.node.open();
		await this.node.connect();
		log.info(`${this.coinName}::Open connection to node`);
		return this.node;
	}

	public startSync(): this {
		if (!this.node) {
			log.error(`${this.coinName}::Can not start sync, because node is not initialized`);
			return;
		} else {
			log.info(`${this.coinName}::Start node sync`);
			this.node.startSync();
		}
		return this;
	}

	public stopSync(): this {
		if (!this.node) {
			log.error(`${this.coinName}::Can not stop sync, because node is not initialized`);
			return;
		} else {
			log.info(`${this.coinName}::Stop node sync`);
			this.node.startSync();
		}
		return this;
	}

	private async stopNode(): Promise<Node> {
		if (!this.node) {
			log.error(`${this.coinName}::Can not stop sync, because node is not initialized`);
			return;
		} else {
			log.info(`${this.coinName}::Stop node sync`);
			this.node.stopSync();
			log.info(`${this.coinName}::Disconnect node`);
			this.node.disconnect();
			log.info(`${this.coinName}::Close node`);
			this.node.close();
		}
		return this;
	}

	public isSynced(): boolean {
		return this.node.chain.synced;
	}

	public getTip(): any {
		return this.node.chain.tip;
	}

	public getBlock(mark: string | number): Observable<bcoin.block> {
		if (typeof mark === `string`) {
			mark = util.revHex(mark);
		} else {
			mark = parseInt(mark as any, 10);
		}
		return Observable.fromPromise(this.node.chain.getBlock(mark))
			.switchMap((block: bcoin.block) => {
				if (block === null || block === undefined) {
					return Observable.throw(new Error(`${this.coinName}::Block with ${+mark ? `height:` : `hash:`} ${mark} not found`));
				} else {
					return Observable
						.combineLatest(
							this.node.chain.getBlockView(block),
							this.node.chain.getHeight(mark)
						)
						.map(data => block.getJSON(this.node.network, data[0], data[1]));
				}
			});
	}

	public on(event, cb): boolean {
		if (this.node) {
			this.node.on(event, cb);
			return true;
		} else {
			log.error(`${this.coinName}::Trying ot subscribe on node event : ${event}, but node is not initialized`);
			return false;
		}
	}

	protected getConfig(): object {

		const nodeConfig: any = config.get(`node`);

		const folderPath: string = nodeConfig.dataFolder + `/` + this.getCoinName();
		const isFolderExist: boolean = fs.existsSync(folderPath);
		if (!isFolderExist) {
			fs.mkdirSync(folderPath);
		}

		return {
			db: `leveldb`,
			network: `testnet`,
			checkpoints: true,
			prefix: folderPath,
			persistent: true,
			logLevel: nodeConfig.logLevel,
			workers: true
		};
	}

	public getCoin(): Coin {
		return this.coin;
	}

	public getCoinName(): string {
		return this.coinName;
	}
}
