import {util} from "bcoin";
import {Observable} from "../../../node_modules/rxjs";
import * as bcoin from "bcoin";
import {Coin} from "../../enum";

export interface NetworkInterface {

	init(): Observable<any>;

	startSync(): this;

	stopSync(): this;

	isSynced(): boolean;

	getBlock(mark: string | number): Observable<bcoin.block>;

	getTip(): any;

	on(event, cb): boolean;

	getCoin(): Coin;

	getCoinName(): string;
}
